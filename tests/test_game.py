import mock
import numpy as np

from classes import Game


def get_size_mock(game):
    return 10, 10


def get_state_mock(game):
    scrap_amount = np.random.randint(1, 4)
    owner = np.random.randint(-1, 2)
    units = (0, np.random.randint(4))[np.random.rand() > 0.7]
    recycler = np.random.randint(3)
    can_build = bool(np.random.randint(2))
    can_spawn = bool(np.random.randint(2))
    in_range_of_recycler = bool(np.random.randint(2))
    return scrap_amount, owner, units, recycler, can_build, can_spawn, in_range_of_recycler


@mock.patch.object(Game, 'get_size', get_size_mock)
def test_game_creation():
    test_game = Game()
    assert test_game.gamer.name == 'Gamer'
